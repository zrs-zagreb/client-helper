#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Install certificate for windows
certutil -addstore -f "ROOT" $DIR/../ssl/server.crt
