#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

function pip_install {
  which pip > /dev/null || sudo apt-get install pip
  for package in "$@"; do
    pip list | grep $package || sudo pip install $package
  done
}

# Install dependent packages
pip_install flask netifaces pyopenssl servicemanager six

# Run tests.
./test-api.sh

# Install and start the service.
../init.d/install.sh
sudo service clienthelper start
