#!/bin/bash
[ "$1" = "-h" ] && echo "Usage: $0 [port] [ip] [--ssl]" && exit 0;

port=8061
ip=127.0.0.1
protocol="http"
[ "$1" != "" ] && port=$1 && shift
[ "$1" != "" ] && ip=$1 && shift
[ "$1" != "" ] && protocol="https"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export SRC="$DIR/../src"
cd $SRC

testdir="clienthelper-testdir"
mkdir $HOME/$testdir
echo "sample code" > $HOME/$testdir/sample.cpp
python clienthelper.py $@ -p $port -i $ip & pid=$!

sleep 1

API_URL="$protocol://$ip:$port"

echo $API_URL

curl ${API_URL}/Identify && \
  echo && echo

curl "${API_URL}/GetFileStatus?directory=${testdir}" && \
  echo && echo

curl "${API_URL}/GetFileData?filename=${testdir}/sample.cpp" && \
  echo && echo

kill $pid || fuser -k $port/tcp
[ "$testdir" != "" ] && rm -rf $HOME/$testdir
echo "Test complete."
