#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
cd ../src
SRC=$(pwd)
cd $DIR

function create_shortcut {
  link=$1
  target=$2
  params=$3  
  echo "Creating shortcut: $link $target"
  # Directories to try to install
  dirs="$HOME "$(find /c/Users/* -maxdepth 0)
  for dir in $dirs; do  
    path="$dir/AppData/Roaming/Microsoft/Windows/Start Menu/Programs/Startup"  
    if [ -d "$path" ]; then
      winpath=$(echo $path | sed 's/^\///' | sed 's/\//\\/g' | sed 's/^./\0:/')
      shortcut //A:c //F:"$winpath\\$link" //T:"$target" //P:"$params" > /dev/null
	  if [ -f "$path/$link" ]; then 
	    echo "Installed shortcut in $dir/.../Startup"
	  else
	    echo "Failed creating shortcut in $dir/.../Startup" 1>&2
	  fi	
    fi
  done
}

export PATH=$PATH:/c/python27:/c/python27/scripts:$DIR

python_bin=$(which pythonw | sed 's/^\///' | sed 's/\//\\/g' | sed 's/^./\0:/').exe
clienthelper=$(echo $SRC/clienthelper.py | sed 's/^\///' | sed 's/\//\\/g' | sed 's/^./\0:/')
gitupdater=$(echo $SRC/gitupdater.py | sed 's/^\///' | sed 's/\//\\/g' | sed 's/^./\0:/')

create_shortcut "client-helper-https.lnk" "$python_bin" "$clienthelper -d -i 0.0.0.0 -p 8443 --ssl"
create_shortcut "client-helper-git-updater.lnk" "$python_bin" "$gitupdater"
