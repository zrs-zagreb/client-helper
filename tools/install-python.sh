#!/bin/bash

if [ "$PROCESSOR_ARCHITECTURE" == "x86" ]; then
  [ ! -f "python-2.7.8.msi" ] && \
    curl -L -o python-2.7.8.msi https://www.python.org/ftp/python/2.7.8/python-2.7.8.msi
  msiexec -i python-2.7.8.msi -q
else
  [ ! -f "python-2.7.8.amd64.msi" ] && \
    curl -L -o python-2.7.8.amd64.msi https://www.python.org/ftp/python/2.7.8/python-2.7.8.amd64.msi
  msiexec -i python-2.7.8.amd64.msi -q
fi

export PATH=$PATH:/c/python27
python --version

