#!/bin/bash
[[ $_ == $0 ]] && echo "Usage: source $0" 1>&2 && exit 127;

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export SRC="$DIR/../src"
cd $DIR

export PATH=$PATH:/c/python27/:/c/python27/Scripts

function install_pip {
  curl -L -o $DIR/get_pip.py https://bootstrap.pypa.io/get-pip.py
  python $DIR/get_pip.py
}

function pip_install {
  which pip > /dev/null || install_pip
  for package in "$@"; do
    pip list | grep $package || pip install $package
  done
}

which python || ./install-python.sh

# Install dependent packages
pip_install flask netifaces pyopenssl servicemanager six

./install-ssl-win.sh
./install-to-startup.sh

netsh advfirewall firewall add rule name="Python" dir=in action=allow program="C:\\python27\\python.exe" enable=yes
netsh advfirewall firewall add rule name="Python" dir=in action=allow program="C:\\python27\\pythonw.exe" enable=yes

python $SRC/clienthelper.py -i 0.0.0.0 -p 8443 --ssl &
sleep 2
start https://localhost:8443/Identify
