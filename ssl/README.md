# Generate the self signed CERT (e.g. on a linux host)

```
cd ~ \
mkdir client-helper-ssl; cd client-helper-ssl; \
rm server.*; \
openssl genrsa -out server.key 4096 && \
openssl req -new -sha256 -key server.key -out server.csr -config openssl.cnf -subj "/C=HR/ST=Grad Zagreb/L=Zagreb/O=Zagrebacki racunalni savez/OU=Development/CN=localhost/emailAddress=zrs@zrs.hr" && \
openssl req -x509 -days 36500 -in server.csr -key server.key -out server.crt -config openssl.cnf && \
openssl x509 -in server.crt -text -noout | grep DNS
```
