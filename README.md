Client Helper
=============

Restricted File System Access API for contest solutions.

This simple Web application built in Python with the [Flask framework](http://flask.pocoo.org/) is used as a
bridge for accessing files (in a predefined directory on a local system) via HTTPS. This is useful in cases
where you have another web application which needs mostly read-only access to a local system but cannot obtain
it unless user interacts in a specific way.

Since this might present a security risk, HTTPS is the suggested default protocol. Other than that, cross-origin requests
are disabled by default in most browsers (at least via XHR) and the App itself only allows access through local loopback (127.0.0.1), therefore
no other machine can actually have direct access to the API. 

After ClientHelper is installed successfully, you should be able to access the API using a browser or any other
HTTP client (e.g. curl) by navigating to https://localhost:8443/

# Installation:

## Windows:

* Install [git SCM](https://git-scm.com/downloads). 
* Check python 2.7.8 or no 2.7.x is installed. **git bash**: `python --version`
* Put git bin directory into system PATH. (Start -> Edit the system environment variables -> PATH -> Edit --> add ; and concat git bin dir path to end of string)
* Open `git bash` as **ADMINISTRATOR**!

### Fast install (including install of python 2.7.8): 

Must run git-bash as admin:

`curl -L -o setup.sh http://bit.ly/client-helper-win && . setup.sh`

### Step by step install:
cd /c/
git clone https://bitbucket.org/zrs-zagreb/client-helper.git
cd client-helper/tools
. setup-win.sh


## Linux:

```
cd ~
git clone  https://bitbucket.org/zrs-zagreb/client-helper.git
cd client-helper/tools
. setup-linux.sh
sudo service clienthelper start
```

---------------------------------------

### Check if running

`chrome https://localhost:8443/Identify`

JSON Output should look smth like (depending on platform):

```
{
  "Date": "2015-12-12 16:15:52", 
  "Interface": "0.0.0.0", 
  "LANIP": "192.168.1.43", 
  "MAC": "10:78:D2:4C:1D:11", 
  "Name": "Chieftec-02", 
  "OS": {
    "system": "Windows", 
    "version": "6.1.7601"
  }, 
  "Port": 8443, 
  "Python-Version": "2.7.0", 
  "Root-Path": "C:\\Users\\Admin\\Desktop", 
  "User": "Admin", 
  "Version": "0.1.9"
}
```

### Optional: Reboot and check if updated.
