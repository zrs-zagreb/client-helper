#!/usr/bin/env python
import unittest
import imp

runner = unittest.TextTestRunner()

try:
  imp.find_module('teamcity')
  from teamcity import is_running_under_teamcity
  from teamcity.unittestpy import TeamcityTestRunner
  if is_running_under_teamcity():
    runner = TeamcityTestRunner()
except ImportError:
  pass

testmodules = [
    'clienthelper_unittest',
]

suite = unittest.TestSuite()

for t in testmodules:
    try:
        # If the module defines a suite() function, call it to get the suite.
        mod = __import__(t, globals(), locals(), ['suite'])
        suitefn = getattr(mod, 'suite')
        suite.addTest(suitefn())
    except (ImportError, AttributeError):
        # else, just load all the test cases from the module.
        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(t))

if __name__ == '__main__':
  runner.run(suite)
