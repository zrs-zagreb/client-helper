def module_exists(module_name):
  try:
    __import__(module_name)
  except ImportError:
    return False
  else:
    return True

if module_exists('netifaces'):
  import netifaces
  
  def get_local_addrs(af_key):
    addrlist = []
    for x in netifaces.interfaces():
      addrs = netifaces.ifaddresses(x)
      if af_key in addrs:
        addrlist.append(addrs[af_key][0]['addr'])  
  
    return addrlist
    
  def get_local_mac_address():
    macs = get_local_addrs(netifaces.AF_LINK)
    if len(macs) > 0:
      if macs[0] == "00:00:00:00:00:00":
        from uuid import getnode as get_mac
	mac = get_mac()
        return ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
      else:
        return macs[0].upper()
    else:
      return "00:00:00:00:00:00"
  
  def get_local_default_ip():
    ips = get_local_addrs(netifaces.AF_INET)
    if len(ips) > 0:
      ips.sort(reverse=True)
      return ips[0]
    else: 
      return "127.0.0.1"
  
else:
  from uuid import getnode
  import socket
  
  def get_local_mac_address():
    mac = getnode()
    hex_mac = ':'.join(("%012X" % mac)[i:i+2] for i in range(0, 12, 2))
    return hex_mac

  def get_local_default_ip():
    hostname = socket.gethostname()
    try:
	  address = socket.gethostbyname("%s.local" % hostname)
    except Exception, e:
	  address = socket.gethostbyname(hostname)

    return address

