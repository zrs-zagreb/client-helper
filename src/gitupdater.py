#!/usr/bin/env python
import logging as log
import os
import platform
import subprocess
import sys
import time

# Platform dependent.

def is_windows():
  return platform.system() == "Windows"

def get_executable():
  if is_windows():
    return "c:\python27\pythonw.exe"
  else:
    return "python"

def get_flags():
  if is_windows() and hasattr(subprocess, 'SW_HIDE'):
    return subprocess.SW_HIDE
  return 0

def parse_cmd(cmd):
  joined = ' '.join(cmd)
  if is_windows():
    return joined.split(' ')
  else:
    return joined

# Platform independent.
def run(cmd):
  p = subprocess.Popen(parse_cmd(cmd),
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE,
                       shell = True,
                       creationflags = get_flags())
  (out, err) = p.communicate()
  for x in out.split("\n"):
    log.info(x)
  for x in err.split("\n"):
    log.error(x)

  return p.returncode == 0

def update():
  cwd = os.getcwd()
  os.chdir(os.path.dirname(os.path.realpath(__file__)))
  log.basicConfig(filename = '../updater.log', level = log.DEBUG)
  log.info("Update started @ " + time.strftime("%Y-%m-%d %H:%M:%S"))
  update_branch = "update-" + time.strftime("%Y%m%d%H%M%S")
  cmd = ["git checkout -b" + update_branch + "&& git pull origin master &&",
         get_executable() + " clienthelper_unittest.py &&",
         "git checkout master && git merge " + update_branch]
  run(cmd)
  run(["git checkout master"])
  run(["git branch -d " + update_branch])
  os.chdir(cwd)
  log.info("Exiting updater @ " + time.strftime("%Y-%m-%d %H:%M:%S"))

if __name__ == '__main__':
  update()
