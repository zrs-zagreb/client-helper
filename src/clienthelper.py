#!/usr/bin/env python
from datetime import datetime
from flask import Flask
from flask import jsonify
from flask import make_response
from flask import request
from flask import send_from_directory
from flask import url_for
from os import listdir
from os.path import basename
from os.path import dirname
from os.path import expanduser
from os.path import isfile, join
from os.path import normpath
import argparse
import errno
import getpass
import hashlib
import OpenSSL
import os.path
import platform
import platformspecific
import socket
import sys
import time
import urllib

def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc: # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(path):
      pass
    else: raise

def md5_file(file_path):
  with open(file_path, 'rb') as fh:
    m = hashlib.md5()
    while True:
      data = fh.read(1024)
      if not data:
        break
      m.update(data)
  return m.hexdigest()


def get_machine_name():
  return socket.gethostname()

def get_python_version():
  return '.'.join(map(str, [sys.version_info.major,
                            sys.version_info.minor,
                            sys.version_info.micro]))

def get_app_version():
  with open (os.path.join(app.root_path, "version.txt"), "r") as version_file:
    return version_file.read().strip()

def get_app_author():
  return "Kristijan Burnik"

def get_app_ip_interface():
  if hasattr(app, 'ip'):
    return app.ip
  else:
    return '127.0.0.1'

def get_app_port():
  if hasattr(app, 'port'):
    return app.port
  else:
    return 8061

def get_mtime(filename):
  return datetime.fromtimestamp(
      os.path.getmtime(filename)).strftime("%Y-%m-%d %H:%M:%S")

def get_site_map():
  output = []
  for rule in app.url_map.iter_rules():
    options = {}
    for arg in rule.arguments:
      options[arg] = "[{0}]".format(arg)

    methods = ','.join(rule.methods)
    url = url_for(rule.endpoint, **options)
    line = urllib.unquote(
        "{:50s} {:20s} {}".format(rule.endpoint, methods, url))
    output.append(line)

  return sorted(output)

################################################################################

app = Flask(__name__)

if not app.debug:
  import logging
  from logging import FileHandler
  from logging import Formatter
  file_handler = FileHandler(os.path.join(app.root_path, "error.log"))
  file_handler.setLevel(logging.ERROR)
  file_handler.setFormatter(Formatter(
      '%(asctime)s %(levelname)s: %(message)s '
      '[in %(pathname)s:%(lineno)d]'
  ))
  app.logger.addHandler(file_handler)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@app.errorhandler(405)
def not_allowed(error):
    return make_response(jsonify({'error': 'Not allowed'}), 405)

@app.errorhandler(500)
def not_allowed(error):
    return make_response(jsonify({'error': 'Request failed'}), 500)

@app.route('/')
def index():
  return make_response(jsonify({
    'App': 'Client Helper',
    'Version': get_app_version(),
    'Site-Map': get_site_map()
  }), 200)

@app.route('/About')
def About():
  return "Client Helper v " + get_app_version() + " by " + get_app_author()

@app.route('/Update')
def Update():
  import gitupdater
  gitupdater.update()
  return make_response(jsonify({
    'Version': get_app_version()
  }), 200)

@app.route('/Identify')
def Identify():
  return make_response(jsonify({
    'MAC': platformspecific.get_local_mac_address(),
    'LANIP': platformspecific.get_local_default_ip(),
    'Name': get_machine_name(),
    'Date': time.strftime("%Y-%m-%d %H:%M:%S"),
    'OS': {'system': platform.system(),
           'version': platform.version()},
    'User': getpass.getuser(),
    'Python-Version': get_python_version(),
    'Version': get_app_version(),
    'Interface': get_app_ip_interface(),
    'Port': get_app_port(),
    'Root-Path': _workPath()
  }), 200)

@app.route('/GetFileStatus', methods = ['GET'])
def GetFileStatus():
  # TODO(kburnik): check in layer below
  if not request.remote_addr == "127.0.0.1":
    return make_response(jsonify({
      'error': 'Not allowed from: ' + str(request.remote_addr)
    }), 400)

  # TODO(kburnik): Check in function.
  home = _workPath()
  path = normpath(home + "/" + request.args['directory'])
  if not path.startswith(home):
    return make_response(jsonify({'error': 'Not allowed path: ' + path}), 400)

  if not os.path.isdir(path):
    return make_response(jsonify({'error': 'Not a directory: ' + path}), 400)

  status = []
  for basename in [ f for f in listdir(path) if isfile(join(path,f)) ]:

    filename = path + "/" + basename;
    status.append({
      'basename': basename,
      'hash': md5_file(filename),
      'size': os.path.getsize(filename),
      'modified': get_mtime(filename)
    })

  return make_response(jsonify({
    'directory': path,
    'status': status
  }))

@app.route('/GetFileData', methods = ['GET'])
def GetFileData():
  # TODO(kburnik): check in layer below
  if not request.remote_addr == "127.0.0.1":
    return make_response(jsonify({
      'error': 'Not allowed from: ' + str(request.remote_addr)
    }), 400)

  # TODO(kburnik): Check in function.
  home = _workPath()
  path = normpath(home + "/" + request.args['filename'])
  if not path.startswith(home):
    return make_response(jsonify({'error': 'Not allowed path: ' + path}), 400)

  filename = path

  if not isfile(filename):
    return make_response(jsonify({'error': 'Not a file: ' + filename}), 400)

  with open(filename, 'r') as file:
    contents = file.read()

  return make_response(jsonify({
      'hash': md5_file(path),
      'size': os.path.getsize(path),
      'modified': get_mtime(path),
      'directory': dirname(path),
      'basename': basename(filename),
      'contents': contents
    }))

@app.route('/CreateIfNotExist', methods = ['POST'])
def CreateIfNotExist():
  home = _workPath()
  path = normpath(home + "/" + request.args['filename'])
  if not path.startswith(home):
    return make_response(jsonify({'error': 'Not allowed path: ' + path}), 400)

  if (os.path.isdir(path)):
    return make_response(jsonify({'error': 'Existing directory: ' + path}), 400)

  mkdir_p(dirname(path))

  if not os.path.isfile(path):
    try:
      with open(path, 'w') as file:
        file.write(request.args['contents'])
    except IOError, e:
      return make_response(jsonify({'error': str(e)}), 400)

  return GetFileData()

@app.route('/favicon.ico')
def favicon():
  return send_from_directory(os.path.join(app.root_path, 'static'),
                             'favicon.ico',
                             mimetype = 'image/vnd.microsoft.icon')

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.cache_control.max_age = 0
    return response

def _workPath():
  home = normpath(expanduser("~"))
  desktop_path = os.path.join(normpath(home), "Desktop")

  if os.path.isdir(desktop_path):
    return desktop_path
  else:
    return home

def start(ip = "127.0.0.1", port = 8061, ssl = None, debug = False):
  if ssl:
    ssl_context = (os.path.join(app.root_path, '../ssl/server.crt'),
                   os.path.join(app.root_path, '../ssl/server.key'))
  else:
    ssl_context = None

  app.ip = ip
  app.port = port
  app.run(host = ip,
          port = app.port,
          debug = debug,
          ssl_context = ssl_context)

if __name__ == '__main__':
  parser = argparse.ArgumentParser(
      description='HTTP(S) based restricted file system access.')
  parser.add_argument('-d', '--debug',
                      action = 'store_true',
                      default = False,
                      help = 'Run in debug mode')
  parser.add_argument('-s', '--ssl',
                      action ='store_true',
                      help = 'Use HTTPS')
  parser.add_argument('-p', '--port',
                      default = 8061,
                      type = int,
                      nargs = '?',
                      help = 'Use specified port',
                      metavar = "port")
  parser.add_argument('-i', '--ip',
                      default = "127.0.0.1",
                      help = 'IP interface to bind to',
                      metavar = "x.x.x.x")
  args = parser.parse_args()

  start(args.ip, args.port, args.ssl, args.debug)
