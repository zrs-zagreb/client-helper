#!/usr/bin/env python
from flask import json, jsonify
from flask import request
from os.path import basename
from os.path import expanduser
from os.path import normpath
import clienthelper
import datetime
import flask
import os
import random
import re
import signal
import subprocess
import time
import unittest

class ClientHelperApiTest(unittest.TestCase):
  def setUp(self):
    clienthelper.app.config['TESTING'] = True
    self.app = clienthelper.app.test_client()

    self.workdir = clienthelper._workPath()
    self.testdir = os.path.join(self.workdir, 'clienthelper-testdir')
    if not os.path.exists(self.testdir):
      os.makedirs(self.testdir)

    open(os.path.join(self.testdir, 'empty.cpp'), 'w').close()

    with open(os.path.join(self.testdir, 'sample1.cpp'), 'wb') as f:
      f.write("One\n")

    with open(os.path.join(self.testdir, 'sample2.cpp'), 'wb') as f:
      f.write('Two\n')

    with open(os.path.join(self.testdir, 'sample3.cpp'), 'wb') as f:
      f.write("Three\n")

    self.test_start_time = self.datetime_to_sec(
        time.strftime("%Y-%m-%d %H:%M:%S"))
    self.kToleranceSeconds = 2


  def datetime_to_sec(self, dt):
    t = datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
    return int((t - datetime.datetime(1970,1,1)).total_seconds())

  def checkRemoteAccessRestriction(self, url):
    response = self.get(url);
    self.assertTrue('error' in response);
    self.assertEqual('Not allowed from: None', response['error']);

    response = self.get(url,
                        environ_base = {'REMOTE_ADDR': '192.168.0.100'})
    self.assertTrue('error' in response);
    self.assertEqual('Not allowed from: 192.168.0.100', response['error']);

    response = self.get(url,
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertTrue(not 'error' in response);

  def assertNear(self, expected, actual, precision):
    self.assertTrue(abs(expected - actual) <= precision)

  def checkHttpHeaders(self, rv):
    expected = {
      'Content-Length': str(len(rv.data)),
      'Content-Type': 'application/json',
      'Cache-Control': 'max-age=0',
      'Access-Control-Allow-Origin': '*'
    }
    headers = {}
    for x in rv.headers:
      headers[x[0]] = x[1]
    self.assertEqual(expected, headers)

  def get(self, route, data = {}, environ_base = {}):
    rv = self.app.get(route, data = data, environ_base = environ_base)
    self.checkHttpHeaders(rv)
    response = json.loads(rv.data)
    return response

  def post(self, route, data = {}, environ_base = {}):
    rv = self.app.post(route, data = data, environ_base = environ_base)
    self.checkHttpHeaders(rv)
    response = json.loads(rv.data)
    return response

  def test_Index(self):
    response = self.get('/')
    keys = ['App', 'Version', 'Site-Map']

    for key in keys:
      self.assertTrue(key in response)

  def test_Identify(self):
    response = self.get('/Identify')
    keys = ['MAC', 'LANIP', 'Name', 'Date', 'OS', 'User', 'Python-Version',
            'Version', 'Port','Interface', 'Root-Path']

    for key in keys:
      self.assertTrue(key in response)

    self.assertTrue('system' in response['OS']);
    self.assertTrue('version' in response['OS']);
    self.assertTrue(response['LANIP'] != '127.0.0.1')
    self.assertTrue(response['MAC'] != '00:00:00:00:00:00')

    self.assertTrue(re.match("[0-9A-F]{2}([-:])[0-9A-F]{2}(\\1[0-9A-F]{2}){4}$",
                             response["MAC"]))
    self.assertEqual(self.workdir, response['Root-Path'])

  def test_CheckRemoteAccessRestriction(self):
    urls = [
      '/GetFileStatus?directory=' + basename(self.testdir),
      '/GetFileData?filename=' + os.path.join(basename(self.testdir), 'empty.cpp')
    ]
    for url in urls:
      self.checkRemoteAccessRestriction(url)


  def test_GetFileStatus(self):
    response = self.get('/GetFileStatus?directory=' + basename(self.testdir),
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    self.assertTrue('status' in response);
    self.assertTrue('directory' in response);

    self.assertEqual(4, len(response['status']))
    self.assertEqual(self.testdir, response['directory'])

    status = sorted(response['status'], key=lambda k: k['basename'])

    expected = [
      {
        'basename':'empty.cpp',
        'hash':'d41d8cd98f00b204e9800998ecf8427e',
        'size':0
      # 'modified' : tested separately.
      },
      {
        'basename':'sample1.cpp',
        'hash':'b602183573352abf933bc7ca85fd0629',
        'size':4
      # 'modified' : tested separately.
      },
      {
        'basename':'sample2.cpp',
        'hash':'3b0ea049a78d4a349993eeeca0c7b508',
        'size':4
      # 'modified' : tested separately.
      },
      {
        'basename':'sample3.cpp',
        'hash':'38a460ffb4cfb15460b4b679ce534181',
        'size':6
      # 'modified' : tested separately.
      },
    ]

    for x in range(0, len(expected)):
      self.assertEqual(expected[x]['basename'], status[x]['basename'])
      self.assertEqual(expected[x]['size'], status[x]['size'])
      self.assertEqual(expected[x]['hash'], status[x]['hash'])
      self.assertNear(self.test_start_time,
                      self.datetime_to_sec(status[x]['modified']),
                      self.kToleranceSeconds)

  def test_GetFileStatus_NotAllowedPath(self):
    response = self.get('/GetFileStatus?directory=../../tmp',
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertEqual({'error': "Not allowed path: /tmp"},
                      response)


  def test_GetFileStatus_NotAllowedPath(self):
    response = self.get('/GetFileStatus?directory=' +
                        basename(self.testdir) + "/empty.cpp",
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertEqual({'error': "Not a directory: " +
                      os.path.join(self.testdir, "empty.cpp")},
                      response)

  def test_GetFileContents(self):
    expected = [
      {
        'basename': 'empty.cpp',
        'contents': '',
        'directory': self.testdir,
        'hash': 'd41d8cd98f00b204e9800998ecf8427e',
        'size': 0
      # 'modified' : tested separately.
      },
      {
        'basename': 'sample1.cpp',
        'contents': 'One\n',
        'directory': self.testdir,
        'hash': 'b602183573352abf933bc7ca85fd0629',
        'size': 4
      # 'modified' : tested separately.
      },
      {
        'basename': 'sample2.cpp',
        'contents': 'Two\n',
        'directory': self.testdir,
        'hash':'3b0ea049a78d4a349993eeeca0c7b508',
        'size': 4
      # 'modified' : tested separately.
      },
      {
        'basename': 'sample3.cpp',
        'contents': 'Three\n',
        'directory': self.testdir,
        'hash': '38a460ffb4cfb15460b4b679ce534181',
        'size': 6
      # 'modified' : tested separately.
      },
    ];

    for i in range(0, len(expected)):
      path = basename(self.testdir) + '/' + expected[i]['basename']
      url = '/GetFileData?filename=' + path
      response = self.get(url, environ_base = {'REMOTE_ADDR': '127.0.0.1'})

      for x in expected[i].keys():
        self.assertEqual(expected[i][x], response[x])

      self.assertNear(self.test_start_time,
                      self.datetime_to_sec(response['modified']),
                      self.kToleranceSeconds)

  def test_GetFileContents_NotAllowedPath(self):
    response = self.get('/GetFileData?filename=../../',
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertEqual({'error': "Not allowed path: " +
    				  normpath(os.path.join(self.workdir, "../../"))},
                      response)


  def test_GetFileContents_NonExistingFile(self):
    response = self.get('/GetFileData?filename=quite_imaginary_file.imext',
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertEqual({'error': "Not a file: " +
                      os.path.join(self.workdir, "quite_imaginary_file.imext")},
                      response)


  def test_GetFileContents_Directory(self):
    response = self.get('/GetFileData?filename=.',
                        environ_base = {'REMOTE_ADDR': '127.0.0.1'})
    self.assertEqual({'error': "Not a file: " +
                      self.workdir},
                      response)

  def test_CreateIfNotExist_NonExistingFile(self):
    response = self.post('/CreateIfNotExist?filename=' +
                         os.path.join(basename(self.testdir),
                         			  "newfile.txt&contents=Hello"),
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    expected = {
      'basename': 'newfile.txt',
      'contents': 'Hello',
      'directory': self.testdir,
      'hash': '8b1a9953c4611296a827abf8c47804d7',
      'size': 5
    # 'modified' : tested separately.
    }

    for x in expected.keys():
      self.assertEqual(expected[x], response[x])

    self.assertNear(self.test_start_time,
                    self.datetime_to_sec(response['modified']),
                    self.kToleranceSeconds)


  def test_CreateIfNotExist_NonExistingFileIntoNestedDirectory(self):
    response = self.post('/CreateIfNotExist?filename=' +
                         basename(self.testdir) +
                         "/a/b/c/d/newfile.txt&contents=Hello",
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    expected = {
      'basename': 'newfile.txt',
      'contents': 'Hello',
      'directory': os.path.join(self.testdir,"a","b","c","d"),
      'hash': '8b1a9953c4611296a827abf8c47804d7',
      'size': 5
    # 'modified' : tested separately.
    }

    for x in expected.keys():
      self.assertEqual(expected[x], response[x])

    self.assertNear(self.test_start_time,
                    self.datetime_to_sec(response['modified']),
                    self.kToleranceSeconds)


  def test_CreateIfNotExist_ExistingFile_DoesNotOverwrite(self):
    response = self.post('/CreateIfNotExist?filename=' +
                         os.path.join(basename(self.testdir),
                         			  "newfile.txt&contents=Hello"),
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    response = self.post('/CreateIfNotExist?filename=' +
                         os.path.join(basename(self.testdir),
                         			  "newfile.txt&contents=ChangedContents"),
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    expected = {
      'basename': 'newfile.txt',
      'contents': 'Hello',
      'directory': self.testdir,
      'hash': '8b1a9953c4611296a827abf8c47804d7',
      'size': 5
    # 'modified' : tested separately.
    }

    for x in expected.keys():
      self.assertEqual(expected[x], response[x])

    self.assertNear(self.test_start_time,
                    self.datetime_to_sec(response['modified']),
                    self.kToleranceSeconds)

  def test_CreateIfNotExist_ExistingFile_FailsOnGet(self):
    response = self.get('/CreateIfNotExist?filename=' +
                         os.path.join(basename(self.testdir),
                         			  "newfile.txt&contents=Hello"),
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    self.assertEqual({'error': 'Not allowed'}, response)


  def test_CreateIfNotExist_FailsForDirectory(self):
    response = self.post('/CreateIfNotExist?filename=' +
                         basename(self.testdir),
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    self.assertEqual({'error': "Existing directory: " + self.testdir},
                      response)

  def test_CreateIfNotExist_FailsForNonUserDirectory(self):
    response = self.post('/CreateIfNotExist?filename=' +
                         "/../../tmp/testfile.txt",
                         environ_base = {'REMOTE_ADDR': '127.0.0.1'})

    self.assertEqual({'error': "Not allowed path: " +
    				  normpath(os.path.join(self.workdir,
    				  						"../../tmp/testfile.txt"))},
                      response)

  def rmdir_r(self, root_path):
    if not root_path.startswith(os.path.join(
          self.workdir, 'clienthelper-testdir')):
      raise Exception("Cannot remove this path: " + root_path)

    for basename in os.listdir(root_path):
      path = os.path.join(root_path, basename)
      if os.path.isdir(path):
        self.rmdir_r(path)
      else:
        os.unlink(path)
    os.rmdir(root_path)

  def tearDown(self):
    self.rmdir_r(self.testdir)

if __name__ == '__main__':
  unittest.main()
